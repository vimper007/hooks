import { useState } from 'react'
import './App.css'
import { NavLink, Route, Routes } from 'react-router-dom'
import UseStateHook from './hooks/UseStateHook'
import UseMemoHook from './hooks/UseMemoHook'
import UseRefHook from './hooks/UseRefHook'
import UseContextHook from './hooks/UseContextHook/UseContextHook'
import UseReducerHook from './hooks/UseReducerHook/UseReducerHook'
import UseCallbackHook from './hooks/UseCallbackHook'
import UseCustomHook from './hooks/UseCustomHook'
import UseLayoutEffectHook from './hooks/UseLayoutEffectHook/UseLayoutEffectHook'
import UseTransitionHook from './hooks/UseTransitionHook'

function App() {

  return (
    <>
    <h1>Hooks</h1>
      <NavLink to='useState'> useState</NavLink>
      <NavLink to='useMemo'> useMemo</NavLink>
      <NavLink to='UseRefHook'> UseRefHook</NavLink>
      <NavLink to='UseContextHook'> UseContextHook</NavLink>
      <NavLink to='UseReducerHook'> UseReducerHook</NavLink>
      <NavLink to='UseCallbackHook'> UseCallbackHook</NavLink>
      <NavLink to='UseCustomHook'> UseCustomHook</NavLink>
      <NavLink to='UseLayoutEffectHook'> UseLayoutEffectHook</NavLink>
      <NavLink to='UseTransitionHook'> UseTransitionHook</NavLink>
      <Routes>
        <Route path='/useState' element={<UseStateHook/>}></Route>
        <Route path='/useMemo' element={<UseMemoHook/>}></Route>
        <Route path='/UseRefHook' element={<UseRefHook/>}></Route>
        <Route path='/UseContextHook' element={<UseContextHook/>}></Route>
        <Route path='/UseReducerHook' element={<UseReducerHook/>}></Route>
        <Route path='/UseCallbackHook' element={<UseCallbackHook/>}></Route>
        <Route path='/UseCustomHook' element={<UseCustomHook/>}></Route>
        <Route path='/UseLayoutEffectHook' element={<UseLayoutEffectHook/>}></Route>
        <Route path='/UseTransitionHook' element={<UseTransitionHook/>}></Route>
      </Routes>
    </>
  )
}

export default App
