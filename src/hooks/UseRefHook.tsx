import React, { ChangeEvent, useEffect, useRef, useState } from 'react'

type Props = {}

const UseRefHook = (props: Props) => {
    const [name, setName] = useState('');
    const renderCount = useRef(1);
    const inputRef = useRef<HTMLInputElement>(null);
    const prevName = useRef('');
    const onChangeSetName = (e:ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
        
    }
    const onClickFocus = () => {
        if(inputRef.current) {
            inputRef.current.focus();
        }
    }
    useEffect(() => {
        renderCount.current++;
    })
    useEffect(()=>{
        prevName.current = name;
        console.log("running name")
        console.log("prevName.current",prevName.current)
    },[name])
    
  return (
    <div>
        <input type="text" ref={inputRef} value={name} onChange={onChangeSetName}/>
        <br />
        <p>My name is {name}</p>
        <br />
        <p>I rendered {renderCount.current} times</p>
        <br />
        <button onClick={onClickFocus}>Focus</button>
        <p>Prev name : {prevName.current}</p>
    </div>
  )
}

export default UseRefHook