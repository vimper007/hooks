import React, { useCallback, useEffect, useState } from 'react'

type Props = {}
const todoArray = [
    'ante blandit viverra. Donec tempus, lorem fringilla ornare placerat, orci',
    'ac turpis egest',
    'ulis quis, pede. Praes',
    'eger aliquam ad'
]
const UseCallbackHook = (props: Props) => {
    // useCallback( [],)
    const [count, setCount] = useState(0);

    // const getTodos = useCallback() => {
    //     return todoArray[count]
    // }
    const getTodos = useCallback(()=>{
        return todoArray[count]

    },[count])
    return (
        <div>
            <button onClick={() => {setCount(prev => prev - 1)}}>-1</button>
            <span>{count}</span>
            <button onClick={() => setCount(prev => prev + 1)}>+1</button>
            <TodoSection getTodos={getTodos}/>
        </div>
    )
}

export default UseCallbackHook

type TodoSectionProps = {
    getTodos: () => string
}

const TodoSection = ({getTodos}:TodoSectionProps) => {
    const [todos, setTodos] = useState<string[]>([])
    useEffect(() => {
      setTodos([...todos,getTodos()])
    }, [getTodos])
    
    return (
        todos.map(todo=>{
            return <h1 key={todo}>
                {todo}
            </h1>
        })
    )
}