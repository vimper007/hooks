import React, { RefObject, useEffect, useRef, useState } from 'react'

type Props = {}

const UseLayoutEffectHook = (props: Props) => {
    const [show, setShow] = useState(false);
    const popup = useRef<HTMLDivElement>(null);
    const button = useRef<HTMLButtonElement>(null);
    useEffect(() => {
        debugger;
      if(popup.current === null || button.current === null) return
      const {bottom} = button?.current?.getBoundingClientRect();
      popup.current.style.top = `${bottom + 25}px`
    }, [show])
    
  return (
    <div>
        <button ref={button} onClick={()=>setShow(prev=>!prev)}>
            Click Here
        </button>
        {show && (
            <div style={{position:'absolute'}} ref={popup}>
                This is a popup
            </div>
        )

        }
    </div>
  )
}

export default UseLayoutEffectHook