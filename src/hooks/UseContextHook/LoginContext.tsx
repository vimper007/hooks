import React, { createContext } from 'react'

type Props = {
    username?: string,
    setUsername?: React.Dispatch<React.SetStateAction<string>>,
    showProfile?: boolean,
    setShowProfile?: React.Dispatch<React.SetStateAction<boolean>>
}

export const LoginContext = createContext<Props>({});