import React, { useState } from 'react'
import { LoginContext } from './LoginContext';
import Profile from './Profile';
import Login from './Login';

type Props = {}

const UseContextHook = (props: Props) => {
  const [username, setUsername] = useState('');
  const [showProfile, setShowProfile] = useState(false)
  return (
    <LoginContext.Provider value={{
      username,
      setUsername,
      showProfile,
      setShowProfile
    }}>
      {
        showProfile ?
          <Profile />
          :
          <Login />
      }
    </LoginContext.Provider>
  )
}

export default UseContextHook