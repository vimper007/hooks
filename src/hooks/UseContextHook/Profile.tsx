import React, { useContext } from 'react'
import { LoginContext } from './LoginContext'

type Props = {}
const Profile = (props: Props) => {
const { username } = useContext(LoginContext)
  return (
    <div>
        <h1>Profile</h1>
        <h2>{username}</h2>
    </div>
  )
}

export default Profile