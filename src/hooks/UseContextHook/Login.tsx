import React, { useContext } from 'react'
import { LoginContext } from './LoginContext'

type Props = {}

const Login = (props: Props) => {
const { username,setUsername,setShowProfile } = useContext(LoginContext)

  return (
    <div>
      <input type="text" name="" value={username} onChange={(e)=>{setUsername?.(e.target.value)}} placeholder='Username'/>
      <br />
      <input type="text" name="" id=""  placeholder='Password'/>
      <br />
      <button onClick={()=>setShowProfile?.(true)}>Login</button>
    </div>
  )
}

export default Login