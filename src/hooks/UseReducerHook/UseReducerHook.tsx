import React, { FormEvent, useReducer, useState } from 'react'
import Todo from './Todo'

type Props = {}

const ACTIONS = {
    ADD: 'add',
    TOGGLE: 'toggle',
    DELETE: 'delete',
}

export type TodoType = {
    id: number,
    todo: string,
    complete: boolean,
}

type ActionType = {
    type: string,
    payload: any,
}

const reducer = (state: TodoType[], action: ActionType) => {
    switch (action.type) {
        case ACTIONS.ADD:
            return [...state, action.payload];
        case ACTIONS.TOGGLE: 
            return [...state.map((todo)=>{
                if(todo.id===action.payload){
                    return{...todo,complete:!todo.complete}
                }
            })];
        case ACTIONS.DELETE: 
            return [...state.filter(todo=>{
                if(todo.id !== action.payload){
                    return todo;
                }
            })]
            
        default:
            return state

    }
}

const UseReducerHook = () => {
    const [state, dispatch] = useReducer(reducer, []);
    const [input, setInput] = useState('');

    const onSubmit = (e:FormEvent) => {
        e.preventDefault();
        dispatch({ type: ACTIONS.ADD, payload: createTodo(input) });
        setInput('')
    }

    const toggler = (id: number) => {
        dispatch({ type: ACTIONS.TOGGLE, payload: id });
    }

    const deleteTodo = (id:number) => {
        dispatch({type:ACTIONS.DELETE,payload:id});
    }

    const createTodo = (input: string) => {
        return {
            id: Date.now(),
            todo: input,
            complete: false,
        }

    }

    return (
        <div>
            <h2>UseReducerHook - Todo</h2>
            <form onSubmit={onSubmit}>
                <input type="text" value={input} onChange={(e) => setInput(e.target.value)} />
                <br />
            </form>
            <Todo todos={state} toggler={toggler} deleteTodo={deleteTodo}/>
        </div>
    )
}

export default UseReducerHook