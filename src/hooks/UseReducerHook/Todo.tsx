import React from 'react'
import { TodoType } from './UseReducerHook'

type Props = {
    todos: TodoType[],
    toggler? :  (id: number) => void
    deleteTodo? :  (id: number) => void
}

const getTodoStyle = (isComplete:boolean) => {
   return isComplete
        ? { textDecoration: 'line-through', color: 'grey' }
        : { textDecoration: 'none', color: 'white' }
}

const Todo = ({ todos,toggler,deleteTodo }: Props) => {
    console.log("Todos:", todos);
    return (
        <>
            {
                todos.map(todo => {
                    return (
                        <div key={todo.id}>
                            <span style={getTodoStyle(todo.complete)}>{todo.todo}</span>
                            <button onClick={()=>toggler?.(todo.id)}>Toggle</button>
                            <button onClick={()=>deleteTodo?.(todo.id)}>Delete</button>
                        </div>
                    )
                })
            }
        </>
    )
}

export default Todo