import React, { ChangeEvent, useState, useTransition } from 'react'

const LIST_SIZE = 20000;

const UseTransitionHook = () => {
    const [input, setInput] = useState('');
    const [list, setList] = useState<string[]>([]);
    const [isPending, startTransiton] = useTransition();
    function handleChange(e: ChangeEvent<HTMLInputElement>) {
        let l = [];
        setInput(e.target.value);
        startTransiton(() => {
            for (let i = 0; i < LIST_SIZE; i++) {
                l.push(input);
                // setList(prev=>prev.push(input))
            }
            setList(l)
        })
    }
    return (
        <div>
            <input type="text" value={input} onChange={handleChange} />
            <br />
            {isPending ? 'loading' : list.map(item => {
                return <p>{item}</p>
            })}
        </div>
    )
}

export default UseTransitionHook