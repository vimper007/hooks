import React, { ChangeEvent, useMemo, useState } from 'react'

type Props = {}

const UseMemoHook = (props: Props) => {
    const [number, setNumber] = useState<string | number>('');
    const [dark, setDark] = useState(false);
    const slowFunction = () => {
        for (let count = 0; count < 1000000000; count++) { }
        return Number(number) * 2;
    }
    const multiply = useMemo(() => slowFunction(), [number])
    const onChangeSetNumber = (e: ChangeEvent<HTMLInputElement>) => {
        setNumber(e.target.value)
    }
    const onClickChangeTheme = () => {
        setDark(!dark)
    }
    const theme = useMemo(() => {
        return {
            backgroundColor: `${dark ? 'black' : 'white'}`,
            color: `${dark ? 'white' : 'black'}`
        }
    }, [dark])

    return (
        <div>
            <input type="number" value={number} onChange={onChangeSetNumber} />
            <br />
            <button style={theme} onClick={onClickChangeTheme}>Change Theme</button>
            <br />
            {multiply}
        </div>
    )
}

export default UseMemoHook