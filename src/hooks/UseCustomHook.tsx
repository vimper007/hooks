import React, { useEffect, useState } from 'react'

type Props = {
    key: 'string',
    initialValue: any
}

const getSavedValue = (key: string, initialValue: any) => {
    const savedValue = localStorage.getItem(key);

    if (savedValue) return savedValue

    if (initialValue instanceof Function) return initialValue();
    return initialValue;
}

const UseLocalStorage = ( key:string, initialValue:any) => {
    const [value, setValue] = useState(() => {
        return getSavedValue(key, initialValue);
    });

    useEffect(()=>{
        localStorage.setItem(key,JSON.stringify(value))
    },[value])

    return [value, setValue];
}

const UseCustomHook = () => {
    const [value,setValue] = UseLocalStorage('name','sxss')
    return(
        <div>
            <h2>Custom hook</h2>
            <button onClick={()=>setValue("ttfd")}>st=et</button>
        </div>
    )
}

export default UseCustomHook